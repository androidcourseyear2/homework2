package com.example.homework2.models;

public class Album extends CellList {
    private long userId;

    public Album(long userId, long id, String title) {
        super(CellListTyper.ALBUM);
        this.userId = userId;
        this.id = id;
        this.title = title;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    private long id;
    private String title;
}
