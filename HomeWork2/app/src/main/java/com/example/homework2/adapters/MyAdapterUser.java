package com.example.homework2.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.homework2.R;
import com.example.homework2.interfaces.OnItemClick;
import com.example.homework2.models.User;

import java.util.ArrayList;

public class MyAdapterUser extends RecyclerView.Adapter<MyAdapterUser.UserViewHolder> {
    private ArrayList<User> users;
    private OnItemClick onItemClick;

    public MyAdapterUser(ArrayList<User> users, OnItemClick onItemClick) {
        this.users = users;
        this.onItemClick = onItemClick;
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater= LayoutInflater.from(parent.getContext());


        View view =inflater.inflate(R.layout.user_item,parent,false);
        UserViewHolder userViewHolder=new UserViewHolder(view);

        return userViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position) {

        User user=(User) users.get(position);
        ((UserViewHolder)holder).bind(user);

        boolean isVisible=users.get(position).isPost();
        if(isVisible==true) {
            holder.postsLayout.setVisibility(View.VISIBLE);
        }else
        {
            holder.postsLayout.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return this.users.size();
    }

    class UserViewHolder extends RecyclerView.ViewHolder{

        private TextView user;
        private TextView userEmail;
        private TextView postText;
        private Button buttonDown;
        private ConstraintLayout postsLayout;
        private View view;

        public UserViewHolder(View view)
        {
            super(view);
            user=view.findViewById(R.id.user);
            userEmail=view.findViewById((R.id.user_email));
            postText=view.findViewById(R.id.postText);
            buttonDown=view.findViewById(R.id.button_down);
            postsLayout=view.findViewById(R.id.posts);
            this.view=view;
        }

        void bind(User userObj)
        {
            user.setText(userObj.getUsername());
            userEmail.setText(userObj.getEmail());

            ArrayList<String> arrayAux=new ArrayList<String>();

            for(int index=0;index<userObj.getPosts().size();index++){
               arrayAux.add(userObj.getPosts().get(index).getTitle());
            }

            String postsConcatenate="";
            for(int indexArray=0;indexArray<arrayAux.size();indexArray++)
            {
                postsConcatenate=postsConcatenate+arrayAux.get(indexArray)+"\n";
            }

            postText.setText(postsConcatenate);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(onItemClick!=null)
                    {
                        onItemClick.userItemClick(userObj);
                    }
                }
            });

            buttonDown.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   if(userObj.isPost()==true)
                   {
                       userObj.setPost(false);
                   }
                   else
                   {
                       userObj.setPost(true);
                   }
                   onItemClick.userItemButtonClick(userObj);

                    notifyItemChanged(getAdapterPosition());

                }
            });


        }
    }
}
