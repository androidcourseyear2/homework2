package com.example.homework2.fragments;

import android.app.DownloadManager;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.homework2.R;
import com.example.homework2.adapters.MyAdapterAlbum;
import com.example.homework2.adapters.MyAdapterUser;
import com.example.homework2.interfaces.FragmentCommunication;
import com.example.homework2.interfaces.OnItemClick;
import com.example.homework2.models.Album;
import com.example.homework2.models.User;
import com.example.homework2.singletons.MySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.example.homework2.constants.Constants.BASE_URL;
import static com.example.homework2.constants.Constants.ID;
import static com.example.homework2.constants.Constants.TITLE;
import static com.example.homework2.constants.Constants.USERID;

public class SecondFragment extends Fragment {
    private FragmentCommunication fragmentCommunication;
    private User user;
    private ArrayList<Album> albums = new ArrayList<Album>();

    //Adapter
    MyAdapterAlbum adapterAlbum = new MyAdapterAlbum(albums, new OnItemClick() {
        @Override
        public void userItemClick(User user) {

        }

        @Override
        public void userItemButtonClick(User user) {

        }

        @Override
        public void albumItemClick(Album album) {

            if (fragmentCommunication != null)
                fragmentCommunication.addThirdFragment(album);
            Toast.makeText(getContext(), "Go to fragment 3", Toast.LENGTH_SHORT).show();
        }
    });

    public SecondFragment(User user) {
        this.user = user;
    }

    public static SecondFragment newInstance(User user) {

        SecondFragment fragment = new SecondFragment(user);
        Bundle args = new Bundle();


        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getAlbums();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_second, container, false);


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        RecyclerView albumList = (RecyclerView) view.findViewById(R.id.album_list);
        albumList.setLayoutManager(linearLayoutManager);
        albumList.setAdapter(adapterAlbum);
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof FragmentCommunication) {
            fragmentCommunication = (FragmentCommunication) context;
        }
    }


    public void getAlbums() {
        MySingleton volleySingleton = MySingleton.getInstance(getContext());
        RequestQueue queue = volleySingleton.getRequestQueue();
        String url = BASE_URL + "/users/";
        url = url + user.getId() + "/albums";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        handleAlbumsResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(getContext(), "Error!", Toast.LENGTH_SHORT).show();
                    }
                });
        queue.add(stringRequest);
    }


    public void handleAlbumsResponse(String response) {
        try {
            user.getAlbums().clear();

            JSONArray albumsJsonArray = new JSONArray(response);

            for (int index = 0; index < albumsJsonArray.length(); index++) {

                JSONObject albumJsonObject = albumsJsonArray.getJSONObject(index);

                if (albumJsonObject != null) {
                    long userId = albumJsonObject.getLong(USERID);
                    long id = albumJsonObject.getLong(ID);
                    String title = albumJsonObject.getString(TITLE);

                    Album album = new Album(userId, id, title);

                    if (user.getAlbums().contains(album) != true) {
                        user.getAlbums().add(album);
                        albums.add(album);
                    }
                }
            }

            adapterAlbum.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }
}
