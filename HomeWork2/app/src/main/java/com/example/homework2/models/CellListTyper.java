package com.example.homework2.models;

public enum CellListTyper {
    USER,
    POST,
    ALBUM,
    PHOTO
}
