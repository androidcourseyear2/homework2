package com.example.homework2.interfaces;

import com.example.homework2.models.Album;
import com.example.homework2.models.User;

public interface FragmentCommunication {

    void addFirstFragment();
    void addSecondFragment(User user);
    void addThirdFragment(Album album);
}
