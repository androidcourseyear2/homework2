package com.example.homework2.models;

import java.util.ArrayList;

public class User extends CellList{

    private long id;

    public User( long id, String name, String username, String email ) {
        super(CellListTyper.USER);
        this.id = id;
        this.name = name;
        this.username = username;
        this.email = email;
        this.post=false;
       this.posts=new ArrayList<Post>();
       this.albums=new ArrayList<Album>();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isPost() {
        return post;
    }

    public void setPost(boolean post) {
        this.post = post;
    }

    public ArrayList<Post> getPosts() {
        return posts;
    }

    public void setPosts(ArrayList<Post> posts) {
        this.posts = posts;
    }

    public ArrayList<Album> getAlbums() {
        return albums;
    }

    public void setAlbums(ArrayList<Album> albums) {
        this.albums = albums;
    }

    private String name;
    private String username;
    private String email;
    private boolean post;
    private ArrayList<Post> posts;
    private ArrayList<Album> albums;
}
