package com.example.homework2.constants;

public class Constants {
   public static  String BASE_URL="https://jsonplaceholder.typicode.com";
   public static String ID="id";
   public static String NAME="name";
   public static String USERNAME="username";
   public static String EMAIL="email";
   public static String ADDRESS="address";
   public static String STREET="street";
   public static String SUITE="suite";
   public static String CITY="city";
   public static String ZIPCODE="zipcode";
   public static String GEO="geo";
   public static String LAT="lat";
   public static String LNG="lng";
   public static String PHONE="phone";
   public static String WEBSITE="website";
   public static String COMPANY="company";
   public static String CATCHPHRASE="catchPhrase";
   public static String BS="bs";
   public static String USERID="userId";
   public static String  TITLE="title";
   public static String  BODY="body";
   public static String ALBUM_ID="albumId";
   public  static String THUMBNAIL_URL="thumbnailUrl";
   public  static String URL="url";

}
