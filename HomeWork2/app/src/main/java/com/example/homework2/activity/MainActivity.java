package com.example.homework2.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.SurfaceControl;

import com.example.homework2.R;
import com.example.homework2.fragments.FirstFragment;
import com.example.homework2.fragments.SecondFragment;
import com.example.homework2.fragments.ThirdFragment;
import com.example.homework2.interfaces.FragmentCommunication;
import com.example.homework2.models.Album;
import com.example.homework2.models.User;

import java.lang.reflect.Field;

public class MainActivity extends AppCompatActivity implements FragmentCommunication {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addFirstFragment();
    }


    @Override
    public void addFirstFragment() {

        FragmentManager fragmentManager=getSupportFragmentManager();
        FragmentTransaction transaction= fragmentManager.beginTransaction();
        String tag= "FirstFragment";
        FragmentTransaction addTransaction= transaction.add(R.id.frame_layout,new FirstFragment(),tag);

        // addTransaction.addToBackStack(tag);

        addTransaction.commit();




    }

    @Override
    public void addSecondFragment(User user) {


        FragmentManager fragmentManager=getSupportFragmentManager();
        FragmentTransaction transaction= fragmentManager.beginTransaction();
        String tag= "SecondFragment";
        FragmentTransaction addTransaction= transaction.add(R.id.frame_layout, SecondFragment.newInstance(user),tag);

        addTransaction.addToBackStack(tag);

        addTransaction.commit();
    }

    @Override
    public void addThirdFragment(Album album) {
        FragmentManager fragmentManager=getSupportFragmentManager();
        FragmentTransaction transaction= fragmentManager.beginTransaction();
        String tag= "ThirdFragment";
        FragmentTransaction addTransaction= transaction.add(R.id.frame_layout, ThirdFragment.newInstance(album),tag);

        addTransaction.addToBackStack(tag);

        addTransaction.commit();

    }
}