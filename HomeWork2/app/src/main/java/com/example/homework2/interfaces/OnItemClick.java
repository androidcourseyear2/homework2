package com.example.homework2.interfaces;

import com.example.homework2.models.Album;
import com.example.homework2.models.User;

public interface OnItemClick {
    public void userItemClick(User user);
    public void userItemButtonClick(User user);
    public void albumItemClick(Album album);
}
