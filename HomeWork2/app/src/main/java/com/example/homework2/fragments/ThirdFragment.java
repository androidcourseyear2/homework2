package com.example.homework2.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.homework2.R;
import com.example.homework2.adapters.MyAdapterPhoto;
import com.example.homework2.interfaces.FragmentCommunication;
import com.example.homework2.models.Album;
import com.example.homework2.models.Photo;
import com.example.homework2.models.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.example.homework2.constants.Constants.ALBUM_ID;
import static com.example.homework2.constants.Constants.ID;
import static com.example.homework2.constants.Constants.THUMBNAIL_URL;
import static com.example.homework2.constants.Constants.TITLE;
import static com.example.homework2.constants.Constants.URL;
import static com.example.homework2.constants.Constants.USERID;

public class ThirdFragment extends Fragment {

    private FragmentCommunication fragmentCommunication;

    private Album album;
    private ArrayList<Photo> photos;

    //adapter
    MyAdapterPhoto adapterPhoto = new MyAdapterPhoto(photos);


    public ThirdFragment(Album album) {
        this.album = album;
    }

    public static ThirdFragment newInstance(Album album) {

        Bundle args = new Bundle();

        ThirdFragment fragment = new ThirdFragment(album);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //getPhotos();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_third, container, false);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 2, RecyclerView.HORIZONTAL, false);

        RecyclerView photoList = (RecyclerView) view.findViewById(R.id.photo_list);
        photoList.setLayoutManager(gridLayoutManager);
        photoList.setAdapter(adapterPhoto);
        return view;
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof FragmentCommunication) {
            fragmentCommunication = (FragmentCommunication) context;
        }
    }

    public void getPhotos() {

    }

    public void handlePhotosResponse(String response) throws JSONException {

        JSONArray photosJsonArray = new JSONArray(response);
        for (int index = 0; index < photosJsonArray.length(); index++) {

            JSONObject photosJsonObject = photosJsonArray.getJSONObject(index);

            if (photosJsonObject != null) {
                long albumId = photosJsonObject.getLong(ALBUM_ID);
                long id = photosJsonObject.getLong(ID);
                String title = photosJsonObject.getString(TITLE);
                String url = photosJsonObject.getString(URL);
                String thumbnailUrl = photosJsonObject.getString(THUMBNAIL_URL);

                Photo photo = new Photo(albumId, id, title, url);

                // to do : verificare daca photo apartine de getPhotos din album ;
            }
        }
        adapterPhoto.notifyDataSetChanged();
    }
}
