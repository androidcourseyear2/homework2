package com.example.homework2.models;

public class CellList {

    private CellListTyper type;

    public CellList(CellListTyper type) {
        this.type = type;
    }

    public CellListTyper getType() {
        return type;
    }

    public void setType(CellListTyper type) {
        this.type = type;
    }
}
