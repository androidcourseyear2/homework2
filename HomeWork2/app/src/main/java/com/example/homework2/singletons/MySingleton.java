package com.example.homework2.singletons;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class MySingleton {

    private static MySingleton singletonInstance;
    private RequestQueue singletonRequestQueue;
    private static Context context;

    private MySingleton(Context context){
        this.context=context;
        this.singletonRequestQueue=getRequestQueue();
    }

    public RequestQueue getRequestQueue() {
        if(singletonRequestQueue==null)
        {
            singletonRequestQueue= Volley.newRequestQueue(context.getApplicationContext());

        }
        return singletonRequestQueue;
    }

    public static synchronized MySingleton getInstance(Context context){
        if(singletonInstance==null){
            singletonInstance=new MySingleton(context);
        }
        return singletonInstance;
    }

    public<T> void addToRequestQueue(Request<T> request){
        getRequestQueue().add(request);
    }
}
